package main

import (
	"Learniary/pkg/cmd"
	"fmt"
	"os"
)

func main() {

	if err := cmd.RunServer(); err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
}