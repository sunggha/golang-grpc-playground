package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

func main() {
	// get configuration
	address := flag.String("server", "http://localhost:8080", "HTTP gateway url, e.g. http://localhost:8080")
	flag.Parse()

	t := time.Now().In(time.UTC)
	pfx := t.Format(time.RFC3339Nano)

	var body string

	// Call Create
	resp, err := http.Post(*address+"/v1/diaries", "application/json", strings.NewReader(fmt.Sprintf(`
		{
			"api":"v1",
			"diary": {
				"dateField":"dateField (%s)",
				"learnedField":"learnedField (%s)",
				"notUnderstandField":"notUnderstandField %s",
				"learnTommorowField":"L T F %s"
			}
		}
	`, pfx, pfx, pfx, pfx)))
	if err != nil {
		log.Fatalf("failed to call Create method: %v", err)
	}
	fmt.Println(resp)
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		body = fmt.Sprintf("failed read Create response body: %v", err)
	} else {
		body = string(bodyBytes)
	}
	log.Printf("Create response: Code=%d, Body=%s\n\n", resp.StatusCode, body)

	// parse ID of created Diary
	var created struct {
		API string `json:"api"`
		ID  int64 `json:"id,string"`
	}
	err = json.Unmarshal(bodyBytes, &created)
	if err != nil {
		log.Fatalf("failed to unmarshal JSON response of Create method: %v", err)
		fmt.Println("error:", err)
	}

	// Call Get
	resp, err = http.Get(fmt.Sprintf("%s%s/%d", *address, "/v1/diaries", created.ID))
	if err != nil {
		log.Fatalf("failed to call Get method: %v", err)
	}
	bodyBytes, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		body = fmt.Sprintf("failed read Read response body: %v", err)
	} else {
		body = string(bodyBytes)
	}
	log.Printf("Read response: Code=%d, Body=%s\n\n", resp.StatusCode, body)

	// Call Update
	req, err := http.NewRequest("PUT", fmt.Sprintf("%s%s/%d", *address, "/v1/diaries", created.ID),
		strings.NewReader(fmt.Sprintf(`
		{
			"api":"v1",
			"diary": {
				"dateField":"date field + updated",
				"learnedField":"learned field (%s) + updated",
				"notUnderstandField":"not understand field (%s) + updated",
				"learnTommorowField":"learn tommorow field (%s) + updated"
			}
		}
	`, pfx, pfx, pfx)))
	req.Header.Set("Content-Type", "application/json")
	//fmt.Println(strings.NewReader(fmt.Sprintf(`
	//	{
	//		"api":"v1",
	//		"diary": {
	//			"dateField":"date field (%s) + updated"
	//			"learnedField":"learned field (%s) + updated",
	//			"notUnderstandField":"not understand field (%s) + updated",
	//			"learnTommorowField":"learn tommorow field (%s) + updated"
	//		}
	//	}
	//`, pfx, pfx, pfx, pfx)))
	//fmt.Println(req)
	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("failed to call Update method: %v", err)
	}
	bodyBytes, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		body = fmt.Sprintf("failed read Update response body: %v", err)
	} else {
		body = string(bodyBytes)
	}
	log.Printf("Update response: Code=%d, Body=%s\n\n", resp.StatusCode, body)

	// Call ReadAll
	resp, err = http.Get(*address + "/v1/diaries")
	if err != nil {
		log.Fatalf("failed to call ReadAll method: %v", err)
	}
	bodyBytes, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		body = fmt.Sprintf("failed read ReadAll response body: %v", err)
	} else {
		body = string(bodyBytes)
	}
	log.Printf("ReadAll response: Code=%d, Body=%s\n\n", resp.StatusCode, body)

	// Call Delete
	req, err = http.NewRequest("DELETE", fmt.Sprintf("%s%s/%d", *address, "/v1/diaries", created.ID), nil)
	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("failed to call Delete method: %v", err)
	}
	bodyBytes, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		body = fmt.Sprintf("failed read Delete response body: %v", err)
	} else {
		body = string(bodyBytes)
	}
	log.Printf("Delete response: Code=%d, Body=%s\n\n", resp.StatusCode, body)
}