package main

import (
	"context"
	"flag"
	"log"
	"time"

	v1 "Learniary/pkg/api/v1"
	"google.golang.org/grpc"
)

const (
	// apiVersion is version of API is provided by server
	apiVersion = "v1"
)

func main() {
	// get configuration
	address := flag.String("server", "", "gRPC server in format host:port")
	flag.Parse()

	// Set up a connection to the server.
	conn, err := grpc.Dial(*address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := v1.NewDiaryServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	t := time.Now().In(time.UTC)
	pfx := t.Format(time.RFC3339Nano)

	// Call Create
	req1 := v1.CreateDiaryReq{
		Api: apiVersion,
		Diary: &v1.Diary{
			DateField			: "dateField ("+pfx+")",
			LearnedField		: "learnedField ("+pfx+")",
			NotUnderstandField	: "notUnderstandField ("+pfx+")",
			LearnTommorowField	: "learnTommorowField ("+pfx+")",
		},
	}
	res1, err := c.CreateDiary(ctx, &req1)
	if err != nil {
		log.Fatalf("Create failed: %v", err)
	}
	log.Printf("Create result: <%+v>\n\n", res1)

	id := res1.Id

	// Read
	req2 := v1.GetDiaryReq{
		Api: apiVersion,
		Id:  id,
	}
	res2, err := c.GetDiary(ctx, &req2)
	if err != nil {
		log.Fatalf("Read failed: %v", err)
	}
	log.Printf("Read result: <%+v>\n\n", res2)

	// Update
	req3 := v1.UpdateDiaryReq{
		Api: apiVersion,
		Diary: &v1.Diary{
			Id:          res2.Diary.Id,
			LearnedField:       res2.Diary.LearnedField + " updated",
			NotUnderstandField	: res2.Diary.NotUnderstandField + " updated",
			LearnTommorowField	: res2.Diary.LearnTommorowField + " updated",
		},
	}
	res3, err := c.UpdateDiary(ctx, &req3)
	if err != nil {
		log.Fatalf("Update failed: %v", err)
	}
	log.Printf("Update result: <%+v>\n\n", res3)

	// Call ReadAll
	req4 := v1.ReadAllDiaryReq{
		Api: apiVersion,
	}
	res4, err := c.ReadAllDiary(ctx, &req4)
	if err != nil {
		log.Fatalf("ReadAll failed: %v", err)
	}
	log.Printf("ReadAll result: <%+v>\n\n", res4)

	// Delete
	req5 := v1.DeleteDiaryReq{
		Api: apiVersion,
		Id:  id,
	}
	res5, err := c.DeleteDiary(ctx, &req5)
	if err != nil {
		log.Fatalf("Delete failed: %v", err)
	}
	log.Printf("Delete result: <%+v>\n\n", res5)
}