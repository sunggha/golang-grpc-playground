package v1

import (
	v1 "Learniary/pkg/api/v1"
	"context"
	"database/sql"
	_ "github.com/lib/pq"
	"fmt"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	// apiVersion is version of API is provided by server
	apiVersion = "v1"
)

// learniaryServiceServer is implementation of v1.LearniaryServiceServer proto interface
type diaryServiceServer struct {
	db *sql.DB
}

// NewLearniaryServiceServer creates Learniary service
func NewLearniaryServiceServer(db *sql.DB) v1.DiaryServiceServer {
	return &diaryServiceServer{db:db}
}

// checkAPI checks if the API version requested by client is supported by server
func (s *diaryServiceServer) checkAPI(api string) error {
	// API version is "" means use current version of the service
	if len(api) > 0 {
		if apiVersion != api {
			return status.Errorf(codes.Unimplemented,
				"unsupported API version: service implements API version '%s', but asked for '%s'", apiVersion, api)
		}
	}
	return nil
}

// connect returns SQL database connection from the pool
func (s *diaryServiceServer) connect(ctx context.Context) (*sql.Conn, error) {
	c, err := s.db.Conn(ctx)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to connect to database-> "+err.Error())
	}
	return c, nil
}

// create new Diary
func (s *diaryServiceServer) CreateDiary(ctx context.Context, req *v1.CreateDiaryReq) (*v1.CreateDiaryRes, error) {
	// check if the API version requested by client is supported by server
	if err := s.checkAPI(req.Api); err != nil {
		return nil, err
	}

	// get SQL connection from pool
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	var id int64
	// create diary entity data
	err = c.QueryRowContext(ctx, "INSERT INTO diaries (dateField, learnedField, notUnderstandField, learnTommorowField) VALUES ($1, $2, $3, $4) RETURNING id", req.Diary.DateField, req.Diary.LearnedField, req.Diary.NotUnderstandField, req.Diary.LearnTommorowField).Scan(&id)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to insert into Diary-> "+err.Error())
	}

	// get ID of creates Diary
	//err = res.Scan(&id)
	//if err != nil {
	//	return nil, status.Error(codes.Unknown, "failed to retrieve id for created Diary-> "+err.Error())
	//}

	return &v1.CreateDiaryRes{
		Api: apiVersion,
		Id: id,
	},nil
}

func (s *diaryServiceServer) GetDiary(ctx context.Context, req *v1.GetDiaryReq) (*v1.GetDiaryRes, error) {
	// check if the API version requested by client is supported by server
	if err := s.checkAPI(req.Api); err != nil {
		return nil, err
	}

	// get SQL connection from pool
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	// query Diary by ID
	rows, err := c.QueryContext(ctx, `
		SELECT
			id,
			dateField,
			learnedField,
			notUnderstandField,
			learnTommorowField
		FROM diaries
		WHERE id = $1;`,
		req.Id)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to select from Diary-> "+err.Error())
	}
	defer rows.Close()

	if !rows.Next() {
		if err := rows.Err(); err != nil {
			return nil, status.Error(codes.Unknown, "failed to retrieve data from Diary-> "+err.Error())
		}
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Diary with ID='%d' is not found",
			req.Id))
	}

	// get Diary data
	var diary v1.Diary
	if err := rows.Scan(
		&diary.Id,
		&diary.DateField,
		&diary.LearnedField,
		&diary.NotUnderstandField,
		&diary.LearnTommorowField); err != nil {
			return nil, status.Error(codes.Unknown, "failed to retrieve field values from Diary row-> "+err.Error())
	}
	if rows.Next() {
		return nil, status.Error(codes.Unknown, fmt.Sprintf("found multiple Diary rows with ID='%d'",
			req.Id))
	}

	return &v1.GetDiaryRes{
		Api:  apiVersion,
		Diary: &diary,
	},nil
}

func (s *diaryServiceServer) UpdateDiary(ctx context.Context, req *v1.UpdateDiaryReq) (*v1.UpdateDiaryRes, error) {
	// check if the API version requested by client is supported by server
	if err := s.checkAPI(req.Api); err != nil {
		return nil, err
	}

	// get SQL connection from pool
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	// update Diary
	res, err := c.ExecContext(ctx, `
		UPDATE diaries 
		SET dateField = $1, learnedField = $2, notUnderstandField = $3, learnTommorowField = $4 
		WHERE id = $5`, req.Diary.DateField, req.Diary.LearnedField, req.Diary.NotUnderstandField, req.Diary.LearnTommorowField, req.Diary.Id)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to update Diary-> "+err.Error())
	}

	rows, err := res.RowsAffected()
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to retrieve rows affected value-> "+err.Error())
	}

	if rows == 0 {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Diary with ID='%d' is not found",
			req.Diary.Id))
	}

	return &v1.UpdateDiaryRes{
		Api:     apiVersion,
		Updated: rows,
	},nil
}

func (s *diaryServiceServer) DeleteDiary(ctx context.Context, req *v1.DeleteDiaryReq) (*v1.DeleteDiaryRes, error) {
	// check if the API version requested by client is supported by server
	if err := s.checkAPI(req.Api); err != nil {
		return nil, err
	}

	// get SQL connection from pool
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	// delete Diary
	res, err := c.ExecContext(ctx, `
		DELETE 
		FROM diaries
		WHERE id = $1`, req.Id)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to delete Diary-> "+err.Error())
	}

	rows, err := res.RowsAffected()
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to retrieve rows affected value-> "+err.Error())
	}

	if rows == 0 {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Diary with ID='%d' is not found",
			req.Id))
	}

	return &v1.DeleteDiaryRes{
		Api:     apiVersion,
		Deleted: rows,
	}, nil
}

func (s *diaryServiceServer) ReadAllDiary(ctx context.Context, req *v1.ReadAllDiaryReq) (*v1.ReadAllDiaryRes, error) {
	// check if the API version requested by client is supported by server
	if err := s.checkAPI(req.Api); err != nil {
		return nil, err
	}

	// get SQL connection from pool
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	// get Diary list
	rows, err := c.QueryContext(ctx, `
		SELECT
			id,
			dateField,
			learnedField,
			notUnderstandField,
			learnTommorowField
		FROM diaries`)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to select from Diaries-> "+err.Error())
	}
	defer rows.Close()

	list := []*v1.Diary{}
	for rows.Next() {
		diary := new(v1.Diary)
		if err := rows.Scan(&diary.Id, &diary.DateField, &diary.LearnedField, &diary.NotUnderstandField, &diary.LearnTommorowField); err != nil {
			return nil, status.Error(codes.Unknown, "failed to retrieve field values from Diary row-> "+err.Error())
		}
		list = append(list, diary)
	}

	if err := rows.Err(); err != nil {
		return nil, status.Error(codes.Unknown, "failed to retrieve data from Diary-> "+err.Error())
	}

	return &v1.ReadAllDiaryRes{
		Api:   apiVersion,
		Diaries: list,
	}, nil
}