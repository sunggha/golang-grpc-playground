package v1

import (
	"context"
	"errors"
	"reflect"
	"testing"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"

	v1 "Learniary/pkg/api/v1"
)

func Test_learniaryServiceServer_Create(t *testing.T) {
	ctx := context.Background()
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	s := NewLearniaryServiceServer(db)
	//tm := time.Now().In(time.UTC)
	//reminder, _ := ptypes.TimestampProto(tm)

	type args struct {
		ctx context.Context
		req *v1.CreateDiaryReq
	}
	tests := []struct {
		name    string
		s       v1.DiaryServiceServer
		args    args
		mock    func()
		want    *v1.CreateDiaryRes
		wantErr bool
	}{
		{
			name: "OK",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.CreateDiaryReq{
					Api: "v1",
					Diary: &v1.Diary{
						DateField			: "dateField",
						LearnedField		: "learnedField",
						NotUnderstandField	: "notUnderstandField",
						LearnTommorowField	: "learnTommorowField",
					},
				},
			},
			mock: func() {
				mock.ExpectQuery("INSERT INTO diaries").WithArgs("dateField", "learnedField", "notUnderstandField","learnTommorowField").
					WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
			},
			want: &v1.CreateDiaryRes{
				Api: "v1",
				Id:  1,
			},
		},
		{
			name: "Unsupported API",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.CreateDiaryReq{
					Api: "v1000",
					Diary: &v1.Diary{
						DateField:       "dateField",
						LearnedField: "learnedField",
						NotUnderstandField:    "notUnderstandField",
						LearnTommorowField: "learnTommorowField",
					},
				},
			},
			mock:    func() {},
			wantErr: true,
		},
		{
			name: "INSERT failed",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.CreateDiaryReq{
					Api: "v1",
					Diary: &v1.Diary{
						DateField			: "dateField",
						LearnedField		: "learnedField",
						NotUnderstandField	: "notUnderstandField",
						LearnTommorowField	: "learnTommorowField",
					},
				},
			},
			mock: func() {
				mock.ExpectExec("INSERT INTO diaries").WithArgs("dateField", "learnedField", "notUnderstandField","learnTommorowField").
					WillReturnError(errors.New("INSERT failed"))
			},
			wantErr: true,
		},
		{
			name: "LastInsertId failed",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.CreateDiaryReq{
					Api: "v1",
					Diary: &v1.Diary{
						DateField			: "dateField",
						LearnedField		: "learnedField",
						NotUnderstandField	: "notUnderstandField",
						LearnTommorowField	: "learnTommorowField",
					},
				},
			},
			mock: func() {
				mock.ExpectExec("INSERT INTO diaries").WithArgs("dateField", "learnedField", "notUnderstandField","learnTommorowField").
					WillReturnResult(sqlmock.NewErrorResult(errors.New("LastInsertId failed")))
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			got, err := tt.s.CreateDiary(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("learniaryServiceServer.CreateDiary() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err == nil && !reflect.DeepEqual(got, tt.want) {
				t.Errorf("learniaryServiceServer.CreateDiary() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_learniaryServiceServer_Get(t *testing.T) {
	ctx := context.Background()
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	s := NewLearniaryServiceServer(db)
	//tm := time.Now().In(time.UTC)
	//reminder, _ := ptypes.TimestampProto(tm)

	type args struct {
		ctx context.Context
		req *v1.GetDiaryReq
	}
	tests := []struct {
		name    string
		s       v1.DiaryServiceServer
		args    args
		mock    func()
		want    *v1.GetDiaryRes
		wantErr bool
	}{
		{
			name: "OK",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.GetDiaryReq{
					Api: "v1",
					Id:  1,
				},
			},
			mock: func() {
				rows := sqlmock.NewRows([]string{"ID", "DateField", "LearnedField", "NotUnderstandField", "LearnTommorowField"}).
					AddRow(1, "dateField", "learnedField", "notUnderstandField", "learnTommorowField")
				mock.ExpectQuery("SELECT (.+) FROM diaries").WithArgs(1).WillReturnRows(rows)
			},
			want: &v1.GetDiaryRes{
				Api: "v1",
				Diary: &v1.Diary{
					Id					: 1,
					DateField			: "dateField",
					LearnedField		: "learnedField",
					NotUnderstandField	: "notUnderstandField",
					LearnTommorowField	: "learnTommorowField",
				},
			},
		},
		{
			name: "Unsupported API",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.GetDiaryReq{
					Api: "v1",
					Id:  1,
				},
			},
			mock:    func() {},
			wantErr: true,
		},
		{
			name: "SELECT failed",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.GetDiaryReq{
					Api: "v1",
					Id:  1,
				},
			},
			mock: func() {
				mock.ExpectQuery("SELECT (.+) FROM diaries").WithArgs(1).
					WillReturnError(errors.New("SELECT failed"))
			},
			wantErr: true,
		},
		{
			name: "Not found",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.GetDiaryReq{
					Api: "v1",
					Id:  1,
				},
			},
			mock: func() {
				rows := sqlmock.NewRows([]string{"ID", "DateField", "LearnedField", "NotUnderstandField", "LearnTommorowField"})
				mock.ExpectQuery("SELECT (.+) FROM diaries").WithArgs(1).WillReturnRows(rows)
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			got, err := tt.s.GetDiary(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("learniaryServiceServer.GetDiary() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if err == nil && !reflect.DeepEqual(got, tt.want) {
				t.Errorf("learniaryServiceServer.GetDiary() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_learniaryServiceServer_Update(t *testing.T) {
	ctx := context.Background()
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	s := NewLearniaryServiceServer(db)
	//tm := time.Now().In(time.UTC)
	//reminder, _ := ptypes.TimestampProto(tm)

	type args struct {
		ctx context.Context
		req *v1.UpdateDiaryReq
	}
	tests := []struct {
		name    string
		s       v1.DiaryServiceServer
		args    args
		mock    func()
		want    *v1.UpdateDiaryRes
		wantErr bool
	}{
		{
			name: "OK",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.UpdateDiaryReq{
					Api: "v1",
					Diary: &v1.Diary{
						Id					: 1,
						DateField			: "new dateField",
						LearnedField		: "new learnedField",
						NotUnderstandField	: "new notUnderstandField",
						LearnTommorowField	: "new learnTommorowField",
					},
				},
			},
			mock: func() {
				mock.ExpectExec("UPDATE diaries").WithArgs("new dateField", "new learnedField", "new notUnderstandField","new learnTommorowField",1).
					WillReturnResult(sqlmock.NewResult(1, 1))
			},
			want: &v1.UpdateDiaryRes{
				Api:     "v1",
				Updated: 1,
			},
		},
		{
			name: "Unsupported API",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.UpdateDiaryReq{
					Api: "v1",
					Diary: &v1.Diary{
						Id					: 1,
						DateField			: "new dateField",
						LearnedField		: "new learnedField",
						NotUnderstandField	: "new notUnderstandField",
						LearnTommorowField	: "new learnTommorowField",
					},
				},
			},
			mock:    func() {},
			wantErr: true,
		},
		{
			name: "UPDATE failed",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.UpdateDiaryReq{
					Api: "v1",
					Diary: &v1.Diary{
						Id					: 1,
						DateField			: "new dateField",
						LearnedField		: "new learnedField",
						NotUnderstandField	: "new notUnderstandField",
						LearnTommorowField	: "new learnTommorowField",
					},
				},
			},
			mock: func() {
				mock.ExpectExec("UPDATE diaries").WithArgs("new dateField", "new learnedField", "new notUnderstandField","new learnTommorowField",1).
					WillReturnError(errors.New("UPDATE failed"))
			},
			wantErr: true,
		},
		{
			name: "RowsAffected failed",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.UpdateDiaryReq{
					Api: "v1",
					Diary: &v1.Diary{
						Id					: 1,
						DateField			: "new dateField",
						LearnedField		: "new learnedField",
						NotUnderstandField	: "new notUnderstandField",
						LearnTommorowField	: "new learnTommorowField",
					},
				},
			},
			mock: func() {
				mock.ExpectExec("UPDATE diaries").WithArgs("new dateField", "new learnedField", "new notUnderstandField","new learnTommorowField",1).
					WillReturnResult(sqlmock.NewErrorResult(errors.New("RowsAffected failed")))
			},
			wantErr: true,
		},
		{
			name: "Not Found",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.UpdateDiaryReq{
					Api: "v1",
					Diary: &v1.Diary{
						Id					: 1,
						DateField			: "new dateField",
						LearnedField		: "new learnedField",
						NotUnderstandField	: "new notUnderstandField",
						LearnTommorowField	: "new learnTommorowField",
					},
				},
			},
			mock: func() {
				mock.ExpectExec("UPDATE diaries").WithArgs("new dateField", "new learnedField", "new notUnderstandField","new learnTommorowField",1).
					WillReturnResult(sqlmock.NewResult(1, 0))
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			got, err := tt.s.UpdateDiary(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("learniaryServiceServer.UpdateDiary() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err == nil && !reflect.DeepEqual(got, tt.want) {
				t.Errorf("learniaryServiceServer.UpdateDiary() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_learniaryServiceServer_Delete(t *testing.T) {
	ctx := context.Background()
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	s := NewLearniaryServiceServer(db)

	type args struct {
		ctx context.Context
		req *v1.DeleteDiaryReq
	}
	tests := []struct {
		name    string
		s       v1.DiaryServiceServer
		args    args
		mock    func()
		want    *v1.DeleteDiaryRes
		wantErr bool
	}{
		{
			name: "OK",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.DeleteDiaryReq{
					Api: "v1",
					Id:  1,
				},
			},
			mock: func() {
				mock.ExpectExec("DELETE FROM diaries").WithArgs(1).
					WillReturnResult(sqlmock.NewResult(1, 1))
			},
			want: &v1.DeleteDiaryRes{
				Api:     "v1",
				Deleted: 1,
			},
		},
		{
			name: "Unsupported API",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.DeleteDiaryReq{
					Api: "v1",
					Id:  1,
				},
			},
			mock:    func() {},
			wantErr: true,
		},
		{
			name: "DELETE failed",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.DeleteDiaryReq{
					Api: "v1",
					Id:  1,
				},
			},
			mock: func() {
				mock.ExpectExec("DELETE FROM diaries").WithArgs(1).
					WillReturnError(errors.New("DELETE failed"))
			},
			wantErr: true,
		},
		{
			name: "RowsAffected failed",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.DeleteDiaryReq{
					Api: "v1",
					Id:  1,
				},
			},
			mock: func() {
				mock.ExpectExec("DELETE FROM diaries").WithArgs(1).
					WillReturnResult(sqlmock.NewErrorResult(errors.New("RowsAffected failed")))
			},
			wantErr: true,
		},
		{
			name: "Not Found",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.DeleteDiaryReq{
					Api: "v1",
					Id:  1,
				},
			},
			mock: func() {
				mock.ExpectExec("DELETE FROM diaries").WithArgs(1).
					WillReturnResult(sqlmock.NewResult(1, 0))
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			got, err := tt.s.DeleteDiary(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("learniaryServiceServer.DeleteDiary() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err == nil && !reflect.DeepEqual(got, tt.want) {
				t.Errorf("learniaryServiceServer.DeleteDiary() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_learniaryServiceServer_ReadAll(t *testing.T) {
	ctx := context.Background()
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	s := NewLearniaryServiceServer(db)
	//tm1 := time.Now().In(time.UTC)
	//reminder1, _ := ptypes.TimestampProto(tm1)
	//tm2 := time.Now().In(time.UTC)
	//reminder2, _ := ptypes.TimestampProto(tm2)

	type args struct {
		ctx context.Context
		req *v1.ReadAllDiaryReq
	}
	tests := []struct {
		name    string
		s       v1.DiaryServiceServer
		args    args
		mock    func()
		want    *v1.ReadAllDiaryRes
		wantErr bool
	}{
		{
			name: "OK",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.ReadAllDiaryReq{
					Api: "v1",
				},
			},
			mock: func() {
				rows := sqlmock.NewRows([]string{"ID", "DateField", "LearnedField", "NotUnderstandField", "LearnTommorowField"}).
					AddRow(1, "dateField 1", "learnedField 1", "notUnderstandField 1", "learnTommorowField 1").
					AddRow(2, "dateField 2", "learnedField 2", "notUnderstandField 2", "learnTommorowField 2")
				mock.ExpectQuery("SELECT (.+) FROM diaries").WillReturnRows(rows)
			},
			want: &v1.ReadAllDiaryRes{
				Api: "v1",
				Diaries: []*v1.Diary{
					{
						Id					: 1,
						DateField			: "dateField 1",
						LearnedField		: "learnedField 1",
						NotUnderstandField	: "notUnderstandField 1",
						LearnTommorowField	: "learnTommorowField 1",
					},
					{
						Id					: 2,
						DateField			: "dateField 2",
						LearnedField		: "learnedField 2",
						NotUnderstandField	: "notUnderstandField 2",
						LearnTommorowField	: "learnTommorowField 2",
					},
				},
			},
		},
		{
			name: "Empty",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.ReadAllDiaryReq{
					Api: "v1",
				},
			},
			mock: func() {
				rows := sqlmock.NewRows([]string{"ID", "DateField", "LearnedField", "NotUnderstandField", "LearnTommorowField"})
				mock.ExpectQuery("SELECT (.+) FROM diaries").WillReturnRows(rows)
			},
			want: &v1.ReadAllDiaryRes{
				Api:   "v1",
				Diaries: []*v1.Diary{},
			},
		},
		{
			name: "Unsupported API",
			s:    s,
			args: args{
				ctx: ctx,
				req: &v1.ReadAllDiaryReq{
					Api: "v1",
				},
			},
			mock:    func() {},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			got, err := tt.s.ReadAllDiary(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("learniaryServiceServer.ReadAllDiary() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("learniaryServiceServer.ReadAllDiary() = %v, want %v", got, tt.want)
			}
		})
	}
}